# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

MY_P=${PN/l/L}
DESCRIPTION="Superkaramba theme and desktop applet for displaying system status"
HOMEPAGE="http://www.kde-look.org/content/show.php/Light?content=51251"
SRC_URI="http://www.kde-look.org/CONTENT/content-files/51251-${MY_P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""

DEPEND=">=kde-base/superkaramba-3.5.5"
RDEPEND="${DEPEND}"

src_install(){
	S="${WORKDIR}/${MY_P}"
	cd ${S}
	insinto /usr/share/themes/superkaramba/${MY_P}
	doins *.png *.theme
}
