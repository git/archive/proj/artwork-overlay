# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

MY_P="lwp"
DESCRIPTION="Superkaramba theme and desktop applet for displaying weather information"
HOMEPAGE="http://liquidweather.net/"
SRC_URI="http://dev.gentoo.org/~cla/artwork/distfiles/${MY_P}-${PV}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE="cups"

DEPEND=">=media-gfx/imagemagick-6.3.3
	>=kde-base/superkaramba-3.5.5
	>=dev-python/PyQt-3.14.1-r2
	cups? ( dev-python/reportlab )"
RDEPEND="${DEPEND}"

src_install(){
	S="${WORKDIR}/${MY_P}-${PV}"
	cd ${S}
	insinto /usr/share/themes/superkaramba/${MY_P}
	doins lwp-14.6.skz
}
