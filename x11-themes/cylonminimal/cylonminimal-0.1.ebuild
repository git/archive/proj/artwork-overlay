# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit kde

MY_P=${P/./}
DESCRIPTION="KDE native window decoration"
HOMEPAGE="http://kde-look.org/content/show.php/'CylonMinimal'+Window+Decoration+for+KDE?content=51993"
SRC_URI="http://kde-look.org/CONTENT/content-files/51993-${MY_P}.tar.gz"

LICENSE="GPL"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""

DEPEND="|| ( kde-base/kwin kde-base/kdebase )"
RDEPEND="${DEPEND}"

need-kde 3.2
