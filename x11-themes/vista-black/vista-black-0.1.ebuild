# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit kde

MY_P=${P/-black/}
DESCRIPTION="KDE native window decoration"
HOMEPAGE="http://kde-look.org/content/show.php/Vista%2BBlack?content=59826"
SRC_URI="http://kde-look.org/CONTENT/content-files/59826-${MY_P}.tar.gz"

LICENSE="GPL"
SLOT="0"
KEYWORDS="~x86"
IUSE=""

DEPEND="|| ( kde-base/kwin kde-base/kdebase )"
RDEPEND="${DEPEND}"

S=${MY_P}

need-kde 3.2
